/*
 * TestTuEStringList.c
 *
 *  Created on: Jan 26, 2015
 *      Author: lazar
 */
#include "__Lib_TStringList.h"

#ifdef __GNUC__
#include "unity.h"

TStringList list = NULL;
int item_index = -1;
Boolean result = FALSE;

void setUp(void)
{
  item_index = -1;
  result = FALSE;
  list = Create();
}
void tearDown(void)
{
//  Destroy(&list);
}

/////////////////////////////////////////////////////////////////////////
/// Create
/////////////////////////////////////////////////////////////////////////

void test_ListShouldBeValidPtrAfterCreation(void)
{
  TEST_ASSERT_NOT_NULL(list);
}

void test_NewlyCreatedListShouldBeEmpty(void)
{
  GetObject(list, 0);
  TEST_ASSERT_EQUAL(UETSL_ERROR_LIST_IS_EMPTY, (int)Error());

  GetString(list, 0);
  TEST_ASSERT_EQUAL(UETSL_ERROR_LIST_IS_EMPTY, (int)Error());

  IndexOf(list, "Is list empty?");
  TEST_ASSERT_EQUAL(UETSL_ERROR_LIST_IS_EMPTY, (int)Error());
}

/////////////////////////////////////////////////////////////////////////
/// Destroy
/////////////////////////////////////////////////////////////////////////

void test_ListShouldBeNullAfterDestruction(void)
{
  Destroy(&list);
  TEST_ASSERT_NULL(list);
}

/////////////////////////////////////////////////////////////////////////
/// Add
/////////////////////////////////////////////////////////////////////////

void test_IndexOfAddedItemsShouldMatchAddingOrder(void)
{
  item_index = Add(list, "First item, index zero");
  TEST_ASSERT_EQUAL(0, item_index);
  TEST_ASSERT_EQUAL_STRING("First item, index zero", GetString(list, 0));

  item_index = Add(list, "Second item, index one");
  TEST_ASSERT_EQUAL(1, item_index);
  TEST_ASSERT_EQUAL_STRING("Second item, index one", GetString(list, 1));

  item_index = Add(list, "Lorem ipsum dolor sit amet...");
  TEST_ASSERT_EQUAL(2, item_index);
  TEST_ASSERT_EQUAL_STRING("Lorem ipsum dolor sit amet...", GetString(list, 2));
}

void test_ItemsAddedToSortedListWillBeAutomaticallySortedInAscendingOrderWithoutTrhowingError(void)
{
  SetSorted(list);

  item_index = Add(list, "Delta");
  TEST_ASSERT_EQUAL(0, item_index);

  item_index = Add(list, "Charlie");
  TEST_ASSERT_EQUAL(0, item_index);

  item_index = Add(list, "Alpha");
  TEST_ASSERT_EQUAL(0, item_index);

  item_index = Add(list, "Echo");
  TEST_ASSERT_EQUAL(3, item_index);

  item_index = Add(list, "Bravo");
  TEST_ASSERT_EQUAL(1, item_index);

  TEST_ASSERT_EQUAL_STRING("Alpha", GetString(list, 0));
  TEST_ASSERT_EQUAL_STRING("Bravo", GetString(list, 1));
  TEST_ASSERT_EQUAL_STRING("Charlie", GetString(list, 2));
  TEST_ASSERT_EQUAL_STRING("Delta", GetString(list, 3));
  TEST_ASSERT_EQUAL_STRING("Echo", GetString(list, 4));

  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}

/////////////////////////////////////////////////////////////////////////
/// AddObject
/////////////////////////////////////////////////////////////////////////

void test_ObjecOfAddedItemShouldMatchObjectWhichIsRetrieved(void)
{
  int IntegerObject = 10;
  uEObject RetrievedObject = NULL;

  AddObject(list, "Item with object", (uEObject)&IntegerObject);
  RetrievedObject = GetObject(list, 0);

  TEST_ASSERT_EQUAL_PTR(&IntegerObject, RetrievedObject);
  TEST_ASSERT_EQUAL(IntegerObject, *((int*)RetrievedObject));
}

void test_WhenItemHasNoObjectNullShouldBeReturned(void)
{
  Add(list, "Item without object");
  TEST_ASSERT_NULL(GetObject(list, 0));
  TEST_ASSERT_EQUAL(UETSL_ERROR_ITEM_HAVE_NO_OBJECT_SET, Error());
}

/////////////////////////////////////////////////////////////////////////
/// Delete
/////////////////////////////////////////////////////////////////////////

void test_DeleteOperationOnEmptyListShouldRaiseListEmpty(void)
{
  Delete(list, 0);
  TEST_ASSERT_EQUAL(UETSL_ERROR_LIST_IS_EMPTY, Error());
}

void test_ItemIsReplacedAfterDeleteAndItemIsNotLast(void)
{
  Add(list, "First item");
  Add(list, "Item for removal");
  Add(list, "Second item");

  Delete(list, 1);

  TEST_ASSERT_EQUAL_STRING("Second item", GetString(list,1));
}


void test_ListShouldBeEmptyAfterDeletingLastItem(void)
{
  Add(list, "First item");
  Add(list, "Second item");
  Add(list, "Third item");

  Delete(list, 0);
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  Delete(list, 0);
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  Delete(list, 0);
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  GetString(list, 0);
  TEST_ASSERT_EQUAL(UETSL_ERROR_LIST_IS_EMPTY, Error());
}

/////////////////////////////////////////////////////////////////////////
/// Clear
/////////////////////////////////////////////////////////////////////////

void test_ClearOperationOnEmptyListShouldRaiseListEmpty(void)
{
  Clear(list);
  TEST_ASSERT_EQUAL(UETSL_ERROR_LIST_IS_EMPTY, Error());
}

void test_ListShouldBeEmptyOnAnyActionAfterClear(void)
{
  Add(list, "First item");
  Add(list, "Second item");
  Add(list, "Third item");

  Clear(list);

  GetString(list, 0);
  TEST_ASSERT_EQUAL(UETSL_ERROR_LIST_IS_EMPTY, Error());
}

/////////////////////////////////////////////////////////////////////////
/// Exchange
/////////////////////////////////////////////////////////////////////////

void
test_TwoItemsShouldBeSwappedAfterExchangeOperation()
{
  Add(list, "First item, will be on index 5");
  Add(list, "Second item, will be on index 3");
  Add(list, "Third item");
  Add(list, "Fourth item will be on index 1");
  Add(list, "Fifth item");
  Add(list, "Sixth item, will be on index 0");

  TEST_ASSERT_EQUAL_STRING("Fourth item will be on index 1", GetString(list, 3));
  TEST_ASSERT_EQUAL_STRING("Second item, will be on index 3", GetString(list, 1));
  TEST_ASSERT_EQUAL_STRING("First item, will be on index 5", GetString(list, 0));
  TEST_ASSERT_EQUAL_STRING("Sixth item, will be on index 0", GetString(list, 5));

  Exchange(list, 1, 3);

  TEST_ASSERT_EQUAL_STRING("Second item, will be on index 3", GetString(list, 3));
  TEST_ASSERT_EQUAL_STRING("Fourth item will be on index 1", GetString(list, 1));

  Exchange(list, 0, 5);

  TEST_ASSERT_EQUAL_STRING("First item, will be on index 5", GetString(list, 5));
  TEST_ASSERT_EQUAL_STRING("Sixth item, will be on index 0", GetString(list, 0));
}

/////////////////////////////////////////////////////////////////////////
/// Find
/////////////////////////////////////////////////////////////////////////

void
test_FindOperationOnNonSortedListShouldSetErrorListIsNotSorted(void)
{
  result = Find(list, "Non-existing string", &item_index);
  TEST_ASSERT_EQUAL(UETSL_ERROR_LIST_IS_NOT_SORTED, Error());
  TEST_ASSERT_FALSE(result);
}

void
test_FindOperationShouldRetrieveFalseSetIndexToZeroIfListIsEmptyAndSetErrorToOk(void)
{
  SetSorted(list);
  // List is empty so any string which is passed sorts at index 0
  result = Find(list, "Non-existing string", &item_index);

  TEST_ASSERT_EQUAL(FALSE, result);
  TEST_ASSERT_EQUAL(0, item_index);
  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}

void
test_FindOperationShouldRetrieveAppropriateResult_SetIndexWhereStringSorts_And_SetErrorToOk(void)
{
  Add(list, "first string");
  Add(list, "second string");

  SetSorted(list);

  result = Find(list, "first string", &item_index);
  TEST_ASSERT_TRUE(result); // Item should be found
  TEST_ASSERT_EQUAL(0, item_index); // Index = 0 because "first string" is at index 0
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  result = Find(list, "third string", &item_index);
  TEST_ASSERT_FALSE(result);
  TEST_ASSERT_EQUAL(2, item_index); // Index = 2 because "third string" sorts after "second string"
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  result = Find(list, "great string", &item_index);
  TEST_ASSERT_FALSE(result);
  TEST_ASSERT_EQUAL(1, item_index); // Index = 1 because "great string" would sort after "first string", if it existed
  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}

/////////////////////////////////////////////////////////////////////////
/// IndexOf
/////////////////////////////////////////////////////////////////////////

void
test_IndexOfOperationShouldRetrieveMinusOneIndexOnEmptyListAndSetErrorToListIsEmpty(void)
{
  item_index = IndexOf(list, "Non-Existing string");
  TEST_ASSERT_EQUAL(-1, item_index);
  TEST_ASSERT_EQUAL(UETSL_ERROR_LIST_IS_EMPTY, Error());
}

void
test_IndexOfOperationShouldObtainThePositionOfTheFirstOccurrenceOfStringAndSetErrorToOk(void)
{
  Add(list, "first string");
  Add(list, "second string");

  item_index = IndexOf(list, "second string");
  TEST_ASSERT_EQUAL(1, item_index);
  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}

void
test_IndexOfOperationShouldActAsFindIfListIsSetSorted(void)
{
  Add(list, "first string");
  Add(list, "second string");

  SetSorted(list);

  item_index = IndexOf(list, "first string");
  TEST_ASSERT_EQUAL(0, item_index); // Index = 0 because "first string" is at index 0
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  item_index = IndexOf(list, "third string");
  TEST_ASSERT_EQUAL(2, item_index); // Index = 2 because "third string" sorts after "second string"
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  item_index = IndexOf(list, "great string");
  TEST_ASSERT_EQUAL(1, item_index); // Index = 1 because "great string" would sort after "first string", if it existed
  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}

void
test_IndexOfShouldReturnMinusOneIfStringDoesNotMatchToAnyStringInListAndShouldSetErrorNoItemAtIndex(void)
{
  Add(list, "first item");
  item_index = IndexOf(list, "Non-existing string");
  TEST_ASSERT_EQUAL(-1, item_index);
  TEST_ASSERT_EQUAL(UETSL_ERROR_NO_ITEM_AT_INDEX, Error());
}

/////////////////////////////////////////////////////////////////////////
/// Insert
/////////////////////////////////////////////////////////////////////////

void
test_InsertShouldInsertStringAtSpecifiedIndexAndSetErrorToOk(void)
{
  Insert(list, 0, "String at index 0");
  Insert(list, 1, "String at index 1");

  TEST_ASSERT_EQUAL_STRING("String at index 0", GetString(list, 0));
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  TEST_ASSERT_EQUAL_STRING("String at index 1", GetString(list, 1));
  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}

void
test_InsertCanInsertItemBetweenTwoItemsWithoutRisingError(void)
{
  Add(list, "String at index 0");
  Add(list, "String which will be at index 2 after insert is done");
  Insert(list, 1, "String at index 1");

  TEST_ASSERT_EQUAL_STRING("String at index 0", GetString(list, 0));
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  TEST_ASSERT_EQUAL_STRING("String at index 1", GetString(list, 1));
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

  TEST_ASSERT_EQUAL_STRING("String which will be at index 2 after insert is done", GetString(list, 2));
  TEST_ASSERT_EQUAL(UETSL_OK, Error());

}

void
test_InsertAtSameIndexInsertsItemsAtReverseOrder(void)
{
  Insert(list, 0, "Item 4");
  Insert(list, 0, "Item 3");
  Insert(list, 0, "Item 2");
  Insert(list, 0, "Item 1");
  Insert(list, 0, "Item 0");

  TEST_ASSERT_EQUAL_STRING("Item 0", GetString(list, 0));
  TEST_ASSERT_EQUAL_STRING("Item 1", GetString(list, 1));
  TEST_ASSERT_EQUAL_STRING("Item 2", GetString(list, 2));
  TEST_ASSERT_EQUAL_STRING("Item 3", GetString(list, 3));
  TEST_ASSERT_EQUAL_STRING("Item 4", GetString(list, 4));
}

void
test_InsertShouldRaiseForbidenOnSortedListErrorIfListIsSorted(void)
{
  SetSorted(list);

  Insert(list, 0, "Insert operation is forbidden on sorted lists");
  TEST_ASSERT_EQUAL(UETSL_ERROR_FORBIDEN_ON_SORTED_LIST, Error());
}


/////////////////////////////////////////////////////////////////////////
/// InsertObject
/////////////////////////////////////////////////////////////////////////

void
test_InsertObjectShouldInsertStringWithObjectAtSpecifiedIndexWithoutRaisingError(void)
{
  int obj1 = 42;
  char *obj2 = "42";

  InsertObject(list, 0, "String with object at index 0", &obj1);
  InsertObject(list, 1, "String with object at index 1", obj2);

  TEST_ASSERT_EQUAL_STRING("String with object at index 0", GetString(list, 0));
  TEST_ASSERT_EQUAL_STRING("String with object at index 1", GetString(list, 1));

  TEST_ASSERT_EQUAL(42, *((int*)GetObject(list, 0)));
  TEST_ASSERT_EQUAL_STRING("42", GetObject(list, 1));

  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}

void
test_InsertObjectCanInsertItemBetweenTwoItemsWithoutRisingError(void)
{
  float obj1 = 18.04;
  int obj2 = 30;

  InsertObject(list, 0, "Item 0", &obj1);
  Add(list, "Item without object, which will be at 2 after insert");
  InsertObject(list, 1, "Item which will be at 1", &obj2);


  TEST_ASSERT_EQUAL_FLOAT(18.04, *((float*)GetObject(list, 0)));
  TEST_ASSERT_EQUAL(30, *((int*)GetObject(list, 1)));
  TEST_ASSERT_EQUAL_STRING("Item without object, which will be at 2 after insert", GetString(list, 2));

  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}

void
test_InsertObjectShouldRaiseForbidenOnSortedListErrorIfListIsSorted(void)
{
  int obj = 42;
  SetSorted(list);

  InsertObject(list, 0, "Insert object operation is forbidden on sorted lists", &obj);
  TEST_ASSERT_EQUAL(UETSL_ERROR_FORBIDEN_ON_SORTED_LIST, Error());
}

/////////////////////////////////////////////////////////////////////////
/// Sort
/////////////////////////////////////////////////////////////////////////

void
test_ListShouldBeSortedCallingSortWithoutRaisingError(void)
{
  item_index = Add(list, "Delta");
  TEST_ASSERT_EQUAL(0, item_index);

  item_index = Add(list, "Charlie");
  TEST_ASSERT_EQUAL(1, item_index);

  item_index = Add(list, "Alpha");
  TEST_ASSERT_EQUAL(2, item_index);

  item_index = Add(list, "Echo");
  TEST_ASSERT_EQUAL(3, item_index);

  item_index = Add(list, "Bravo");
  TEST_ASSERT_EQUAL(4, item_index);

  Sort(list);

  TEST_ASSERT_EQUAL_STRING("Alpha", GetString(list, 0));
  TEST_ASSERT_EQUAL_STRING("Bravo", GetString(list, 1));
  TEST_ASSERT_EQUAL_STRING("Charlie", GetString(list, 2));
  TEST_ASSERT_EQUAL_STRING("Delta", GetString(list, 3));
  TEST_ASSERT_EQUAL_STRING("Echo", GetString(list, 4));

  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}

void
test_ListShouldBeSortedAfterAddingItemsAndEnablingSortedPorpery(void)
{
  item_index = Add(list, "Vjedi");
  TEST_ASSERT_EQUAL(0, item_index);

  item_index = Add(list, "Az");
  TEST_ASSERT_EQUAL(1, item_index);

  item_index = Add(list, "Buki");
  TEST_ASSERT_EQUAL(2, item_index);

  SetSorted(list);
  TEST_ASSERT_EQUAL_STRING("Az", GetString(list, 0));
  TEST_ASSERT_EQUAL_STRING("Buki", GetString(list, 1));
  TEST_ASSERT_EQUAL_STRING("Vjedi", GetString(list, 2));
}

void
test_ListWithOneItemShouldBeTheSameAfterSortAndRaiseNoError(void)
{
  Add(list, "only item");
  Sort(list);

  TEST_ASSERT_EQUAL_STRING("only item", GetString(list, 0));
  TEST_ASSERT_EQUAL(UETSL_OK, Error());
}


#else

// uC tests

#include <stdlib.h>

void main(void)
{
 TStringList list = NULL;
 char *str_obj = "objekat";
 char *str = NULL;

 uEStringListError last_error = UETSL_OK;

 MM_Init();

 list = Create();

 // item 0
 Add(list, "Test");
 last_error = Error();

 // item 1
 AddObject(list, "Test sa objektom", (uEObject)str_obj);
 last_error = Error();

 // item 2
 Add(list, "Test za brisanje");
 last_error = Error();

 // item 3
 AddObject(list, "Test koji se ne briše", (uEObject)&last_error);
 last_error = Error();

 Delete(list, 2);
 last_error = Error();

 // item 3
 Add(list, "This will avoid deletion");;
 last_error = Error();

 // item 4
 Add(list, "This will be deleted");;
 last_error = Error();

 Exchange(list, 3, 4);
 last_error = Error();

 str = GetString(list, 3);
 last_error = Error();

 str = GetString(list, 4);
 last_error = Error();

 Delete(list, 3);
 last_error = Error();

 Clear(list);
 last_error = Error();
}

#endif // __GNUC__
