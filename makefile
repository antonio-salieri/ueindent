CLEANUP = rm -fv out/*
MKDIR = mkdir -p
TEST_TARGET_EXTENSION=.out

UNITY_ROOT=test/unity

C_COMPILER=gcc

CFLAGS=-std=c99 -fPIC -c -shared
TEST_CFLAGS=-std=c99

TEST_TARGET_BASE=out/test
TARGET=out/ue_string_list.so
TEST_TARGET = $(TEST_TARGET_BASE)$(TEST_TARGET_EXTENSION)

SRC_FILES=src/__Lib_TStringList.c
TEST_SRC_FILES=$(UNITY_ROOT)/src/unity.c test/tests/TestTuEStringList.c  test/tests/test_runners/TestTuEStringList_Runner.c

INC_DIRS=-Isrc
TEST_INC_DIRS=$(INC_DIRS) -I$(UNITY_ROOT)/src

SYMBOLS=
TEST_SYMBOLS=-DTEST

all: clean lib tests

lib:
	$(C_COMPILER) $(CFLAGS) $(INC_DIRS) $(SYMBOLS) $(SRC_FILES) -o $(TARGET)

tests:
#	ruby $(UNITY_ROOT)/auto/generate_test_runner.rb test/tests/TestTuEStringList.c  test/tests/test_runners/TestTuEStringList_Runner.c
	$(C_COMPILER) $(TEST_CFLAGS) $(TEST_INC_DIRS) $(TEST_SYMBOLS) $(TEST_SRC_FILES) -l:out/ue_string_list.so -o $(TEST_TARGET)
	./$(TEST_TARGET)

clean:
	@$(CLEANUP)

