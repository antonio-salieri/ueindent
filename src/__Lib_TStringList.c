/*
 * TestTuEStringList.c
 *
 *  Created on: Jan 26, 2015
 *      Author: lazar
 */

#include "__Lib_TStringList.h"

#include <stdlib.h>
#include <string.h>

#define RAISE_ERROR_ALLOCATING_MEMORY() _set_error(UETSL_ERROR_ALLOCATING_MEMORY)

#define HAS_NEXT(list_item) (list_item->next)
#define TO_LIST(list) (_TStringList)list
#define LIST_IS_EMPTY(list) (list && (list->length == 0 || list->root == NULL || list->lastItem == NULL))

#ifdef __GNUC__
  #define UETSL_FREE(var) free((void*)var)
  #define UETSL_FREE_LIST_ITEM(item) free((void*)item->string->content); \
                                    free((void*)item->string); \
                                    free((void*)item)
#else
  #define UETSL_FREE(var) Free((unsigned char*)var, sizeof(*var))
  #define UETSL_FREE_LIST_ITEM(item) memset(item->string->content, 0, item->string->len); \
                                    Free((unsigned char*)item->string->content, item->string->len); \
                                    memset(item->string, 0, sizeof(*item->string)); \
                                    Free((unsigned char*)item->string, sizeof(*item->string)); \
                                    memset(item, 0, sizeof(*item)); \
                                    Free((unsigned char*)item, sizeof(*item))
#endif // __GNUC__

static uEStringListError _error = UETSL_OK;
static uEStringListError _last_error = UETSL_OK;

struct sTuEStringObject {
  uEString content;
  unsigned long len;
};
typedef struct sTuEStringObject* TString;

typedef struct sTuEStringListItem* TStringListItem;
struct sTuEStringListItem
{
  TStringListItem next;
  TStringListItem prev;
  TString string;
  uEObject *object;
};

struct sTuEStringList
{
  unsigned long length;
  TStringListItem root;
  TStringListItem lastItem;
  Boolean sorted;
};
typedef struct sTuEStringList *_TStringList;

static TStringListItem
_new_string_list_item(const uEString String, uEObject Object);

static TStringListItem
_find_item_at_index_silently(const _TStringList list, int index);

static TStringListItem
_find_item_at_index(const _TStringList list, int index);

static void
_insert_object_at_index(TStringList StringList,
                        int Index,
                        const uEString String,
                        uEObject Object);

static void
_set_error(uEStringListError error);

static Boolean
_find_string_index_in_list(TStringListItem item,
                            const uEString String,
                            int* Index);

static int
_add_object(_TStringList list, const uEString String, uEObject Object);

static int
_item_cmp(const TStringListItem item1, const TStringListItem item2);

static void
_exchange_list_items(_TStringList list, const TStringListItem item1, const TStringListItem item2);

TStringList
Create(void)
{
  _TStringList list = NULL;

  list = (_TStringList) Malloc(sizeof(*list));
  if (!list)
  {
    RAISE_ERROR_ALLOCATING_MEMORY();
    return NULL;
  }

  list->length = 0;
  list->root = NULL;
  list->lastItem = NULL;
  list->sorted = FALSE;

  return list;
}

void
Destroy(TStringList *StringList)
{
  _TStringList list;

  if (!*StringList)
    return;

  list = TO_LIST(*StringList);
  Clear(list);

  Free((unsigned char*)list, sizeof(*list));
  *StringList = NULL;

  _set_error(UETSL_OK);
}

int
Add(TStringList StringList, const uEString String)
{
  return AddObject(StringList, String, NULL);
}

int
AddObject(TStringList StringList, const uEString String, uEObject Object)
{
  _TStringList list = TO_LIST(StringList);
  int position = 0;

  if (GetSorted(StringList))
  {
    Find(StringList, String, &position);
    _insert_object_at_index(StringList, position, String, Object);
  }
  else
  {
    position = _add_object(list, String, Object);
  }

  return position;
}

void
Clear(TStringList StringList)
{
  _TStringList list = TO_LIST(StringList);
  TStringListItem item;
  TStringListItem prev_item = NULL;

  if (!list)
  {
    _set_error(UETSL_ERROR_INVALID_LIST);
    return;
  }

  if (LIST_IS_EMPTY(list))
  {
    _set_error(UETSL_ERROR_LIST_IS_EMPTY);
    return;
  }

  item = list->lastItem;

  while(item)
  {
    prev_item = item->prev;
    UETSL_FREE_LIST_ITEM(item);
    item = prev_item;
    list->lastItem = item;
    list->length--;
  }
  list->root = item;

  if (list->length != 0)
  {
    _set_error(UETSL_INTERNAL_ERROR_ITEM_COUNT_NOT_ZERO);
    return;
  }

  if (list->lastItem)
  {
    _set_error(UETSL_INTERNAL_ERROR_LAST_ITEM_STILL_EXISTS);
  }
}

void
Delete(TStringList StringList, int Index)
{
  _TStringList list = TO_LIST(StringList);
  TStringListItem item = NULL;
  TStringListItem next_item = NULL;
  TStringListItem prev_item = NULL;

  item = _find_item_at_index(list, Index);
  if (!item)
    return;

  if (!item)
  {
    _set_error(UETSL_ERROR_NO_ITEM_AT_INDEX);
    return;
  }
  if (!item->prev)
  {
    list->root = list->root->next;
    if (list->root)
      list->root->prev = NULL;
  }
  else
  {
    prev_item = item->prev;
  }

  if (!item->next)
  {
    list->lastItem = item->prev;
    if(list->lastItem)
      list->lastItem->next = NULL;
  }
  else
  {
    next_item = item->next;
  }

  if (prev_item)
    prev_item->next = next_item;

  if(next_item)
    next_item->prev = prev_item;

  UETSL_FREE_LIST_ITEM(item);
  list->length--;


}

void Exchange(TStringList StringList, int Index1, int Index2)
{
  _TStringList list = TO_LIST(StringList);
  TStringListItem item1 = NULL;
  TStringListItem item2 = NULL;

  if (Index1 == Index2)
    return;

  item1 = _find_item_at_index(list, Index1);
  if (!item1)
    return;

  item2 = _find_item_at_index(list, Index2);
  if (!item2)
    return;

  _exchange_list_items(list, item1, item2);
}

Boolean
Find(TStringList StringList, const uEString String, int *Index)
{
  _TStringList list = TO_LIST(StringList);
  volatile TStringListItem item = NULL;
  *Index = 0;

  if (!GetSorted(StringList))
  {
    _set_error(UETSL_ERROR_LIST_IS_NOT_SORTED);
    return FALSE;
  }
  item = list->root;
  return _find_string_index_in_list(item, String, Index);
}

void
SetSorted(TStringList StringList)
{
  _TStringList list = TO_LIST(StringList);
  Sort(StringList);
  list->sorted = TRUE;
}

Boolean
GetSorted(TStringList StringList)
{
  _TStringList list = TO_LIST(StringList);
  return list->sorted;
}

int
IndexOf(TStringList StringList, const uEString String)
{
  _TStringList list = TO_LIST(StringList);
  int index = -1;

  if (LIST_IS_EMPTY(list))
  {
    _set_error(UETSL_ERROR_LIST_IS_EMPTY);
    return index;
  }
  if (GetSorted(list))
  {
    Find(StringList, String, &index);
  }
  else
  {
    if (_find_string_index_in_list(list->root, String, &index))
    {
      index = 1;
    }
    else
    {
      _set_error(UETSL_ERROR_NO_ITEM_AT_INDEX);
    }
  }

  return index;
}

void
InsertObject(TStringList StringList,
            int Index,
            const uEString String,
            uEObject Object)
{
  if (GetSorted(StringList))
  {
    _set_error(UETSL_ERROR_FORBIDEN_ON_SORTED_LIST);
    return;
  }

  _insert_object_at_index(StringList,
                          Index,
                          String,
                          Object);
}

void
Insert(TStringList StringList,
      int Index,
      const uEString String)
{
  InsertObject(StringList, Index, String, NULL);
}

void
Sort(TStringList StringList)
{
  _TStringList list = TO_LIST(StringList);

  TStringListItem sublist1_element, sublist2_element, next_element, tail, root;
  int insize, merges_count, sublist1_size, sublist2_size, i;

  root = list->root;
  if (!root)
    return;

  insize = 1;

  while (1)
  {
    sublist1_element = root;
    root = NULL;
    tail = NULL;
    merges_count = 0;

    while (sublist1_element)
    {
      merges_count++;
      sublist2_element = sublist1_element;
      sublist1_size = 0;
      for (i = 0; i < insize; i++)
      {
        sublist1_size++;
        sublist2_element = sublist2_element->next;
        if (!sublist2_element)
          break;
      }

      sublist2_size = insize;
      while (sublist1_size > 0 || (sublist2_size > 0 && sublist2_element))
      {
        if (sublist1_size == 0)
        {
          next_element = sublist2_element;
          sublist2_element = sublist2_element->next;
          sublist2_size--;
        }
        else if (sublist2_size == 0 || !sublist2_element)
        {
          next_element = sublist1_element;
          sublist1_element = sublist1_element->next;
          sublist1_size--;
        }
        else if (_item_cmp(sublist1_element, sublist2_element) < 0)
        {
          next_element = sublist1_element;
          sublist1_element = sublist1_element->next;
          sublist1_size--;
        }
        else
        {
          next_element = sublist2_element;
          sublist2_element = sublist2_element->next;
          sublist2_size--;
        }

        if (tail)
          tail->next = next_element;
        else
          root = next_element;

        next_element->prev = tail;
        tail = next_element;
      }

      sublist1_element = sublist2_element;
    }
    tail->next = NULL;

    if (merges_count <= 1)
    {
      list->root = root;
      list->lastItem = tail;
      return;
    }

    /* Otherwise repeat, merging lists twice the size */
    insize *= 2;
  }
}

/**
 * Note: Do not call this function internally, because it will
 *  reset the _error and it will not be available anymore
 *  outside of module.
 *
 * @return
 */
uEStringListError
Error(void)
{
  _last_error = _error;
  _error = UETSL_OK;
  return _last_error;
}

const uEString
GetString(TStringList StringList, int Index)
{
  _TStringList list = TO_LIST(StringList);
  TStringListItem item = _find_item_at_index(list, Index);
  if(!item)
    return NULL;


  if (!item)
  {
    _set_error(UETSL_ERROR_NO_ITEM_AT_INDEX);
    return NULL;
  }

  if (!item->string)
  {
    _set_error(UETSL_ERROR_ITEM_HAVE_NO_STRING_SET);
    return NULL;
  }

  if (!item->string->content)
  {
    _set_error(UETSL_ERROR_ITEM_HAVE_NO_STRING_SET);
    return NULL;
  }

  return item->string->content;
}

const uEObject
GetObject(TStringList StringList, int Index)
{
  _TStringList list = TO_LIST(StringList);
  TStringListItem item = _find_item_at_index(list, Index);
  if(!item)
    return NULL;

  if (!item)
  {
    _set_error(UETSL_ERROR_NO_ITEM_AT_INDEX);
    return NULL;
  }

  if (!item->object)
  {
    _set_error(UETSL_ERROR_ITEM_HAVE_NO_OBJECT_SET);
    return NULL;
  }

  return item->object;
}

static TStringListItem
_new_string_list_item(const uEString String, uEObject Object)
{
  TStringListItem item = NULL;

  item = (TStringListItem) Malloc(sizeof(*item));
  if (!item)
  {
    RAISE_ERROR_ALLOCATING_MEMORY();
    return item;
  }

  item->object = NULL;
  item->string = NULL;
  item->next = NULL;
  item->prev = NULL;

  if (String)
  {
    item->string = (TString) Malloc(sizeof(*item->string));
    if (!item->string)
    {
      RAISE_ERROR_ALLOCATING_MEMORY();
      UETSL_FREE(item);
      return item;
    }

    item->string->len = strlen(String) + 1;
    item->string->content = (uEString) Malloc(item->string->len);
    memset(item->string->content, 0, item->string->len);

    if (!item->string)
    {
      RAISE_ERROR_ALLOCATING_MEMORY();
      UETSL_FREE(item->string);
      UETSL_FREE(item);
      return item;
    }
    memcpy(item->string->content, String, item->string->len);
  }

  if (Object)
  {
    item->object = Object;
  }

  return item;
}

static TStringListItem
_find_item_at_index_silently(const _TStringList list, int index)
{
  TStringListItem item = NULL;
  int curent_position = 0;

  item = list->root;
  curent_position = 0;
  while(curent_position < index)
  {
    if (!HAS_NEXT(item))
      break;

    item = item->next;
    curent_position++;
  }

  if (curent_position != index)
    item = NULL;

  return item;
}

static TStringListItem
_find_item_at_index(const _TStringList list, int index)
{
  if (LIST_IS_EMPTY(list))
  {
    _set_error(UETSL_ERROR_LIST_IS_EMPTY);
    return NULL;
  }

  return _find_item_at_index_silently(list, index);
}

static void
_insert_object_at_index(TStringList StringList,
                        int Index,
                        const uEString String,
                        uEObject Object)
{
  _TStringList list = TO_LIST(StringList);

  TStringListItem new_item = NULL;
  TStringListItem next_item = NULL;
  TStringListItem prev_item = NULL;

  new_item = _new_string_list_item(String, Object);
  if (!new_item)
    return;

  next_item = _find_item_at_index_silently(list, Index);
  if (!next_item)
  {
    _add_object(list, String, Object);
    return;
  }

  if (next_item)
    prev_item = next_item->prev;

  new_item->next = next_item;
  new_item->prev = prev_item;

  if (next_item)
    next_item->prev = new_item;

  if (prev_item)
    prev_item->next = new_item;

  if (!new_item->next)
    list->lastItem = new_item;
  if (!new_item->prev)
    list->root = new_item;
  list->length++;
}

static void
_set_error(uEStringListError error)
{
  if (error < _UE_STRING_LIST_ERROR_COUNT_)
  {
    _error = error;
  }
}

static Boolean
_find_string_index_in_list(TStringListItem item,
                          const uEString String,
                          int* Index)
{
  Boolean result = FALSE;

  while (item)
  {
    if (item->string)
    {
      if (item->string->content)
      {
        if (strncmp(String, item->string->content, item->string->len) == 0)
        {
          result = TRUE;
          break;
        }
        else if (strcmp(String, item->string->content) < 0)
        {
          break;
        }
      }
    }
    (*Index)++;
    item = item->next;
  }
  return result;
}

static int
_add_object(_TStringList list, const uEString String, uEObject Object)
{
  TStringListItem new_item = NULL;
  TStringListItem parent = NULL;
  int position = 0;

  position = list->length;

  new_item = _new_string_list_item(String, Object);
  if (!new_item)
    return 0;

  parent = list->lastItem;

  if (!parent)
  {
    list->root = new_item;
  }
  else
  {
    if (parent->next)
    {
      _set_error(USTSL_INTERNAL_ERROR_INVALID_LAST_ITEM);
      return 0;
    }
    parent->next = new_item;
    new_item->prev = parent;
  }

  list->lastItem = new_item;
  list->length++;

  return position;
}

static int
_item_cmp(const TStringListItem item1, const TStringListItem item2)
{
  int result = 0;
  TString str1, str2;

  if (!item1)
    result = -1;
  else
    str1 = item1->string;

  if (!item2)
    result = 1;
  else
    str2 = item2->string;

  if (str1->len > 0 && str2->len > 0)
    result = strcmp(str1->content, str2->content);

  return result;
}

static void
_exchange_list_items(_TStringList list, const TStringListItem item1, const TStringListItem item2)
{
  TStringListItem temp_next = NULL;
  TStringListItem temp_prev = NULL;

  temp_next = item1->next;
  temp_prev = item1->prev;

  if (list->root == item1)
    list->root = item2;
  else if (list->root == item2)
    list->root = item1;

  if (list->lastItem == item1)
    list->lastItem = item2;
  else if (list->lastItem == item2)
    list->lastItem = item1;

  item1->next = item2->next;
  item1->prev = item2->prev;
  if (item1->next)
    item1->next->prev = item1;
  if (item1->prev)
    item1->prev->next = item1;

  item2->next = temp_next;
  item2->prev = temp_prev;
  if (item2->next)
    item2->next->prev = item2;
  if (item2->prev)
    item2->prev->next = item2;
}
