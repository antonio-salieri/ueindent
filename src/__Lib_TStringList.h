/*
 * TestTuEStringList.c
 *
 *  Created on: Jan 26, 2015
 *      Author: lazar
 */

#ifndef T_UE_STRING_LIST_H_
#define T_UE_STRING_LIST_H_

#ifdef __GNUC__
  #define Free(ptr, size) free((void*)ptr)
  #define Malloc(size) malloc(size)
  #define MM_Init()   asm("nop")
#endif // __GNUC__

typedef enum {
  UETSL_OK,
  UETSL_ERROR_INVALID_LIST,
  UETSL_ERROR_ALLOCATING_MEMORY,
  UETSL_ERROR_NO_ITEM_AT_INDEX,
  UETSL_ERROR_ITEM_HAVE_NO_STRING_SET,
  UETSL_ERROR_ITEM_HAVE_NO_OBJECT_SET,
  UETSL_ERROR_LIST_IS_EMPTY,
  UETSL_ERROR_LIST_IS_NOT_SORTED,
  UETSL_ERROR_FORBIDEN_ON_SORTED_LIST,

  UETSL_INTERNAL_ERROR_ITEM_COUNT_NOT_ZERO = 0x80,
  UETSL_INTERNAL_ERROR_LAST_ITEM_STILL_EXISTS,
  USTSL_INTERNAL_ERROR_INVALID_LAST_ITEM,

  _UE_STRING_LIST_ERROR_COUNT_
} uEStringListError;

typedef enum {
        FALSE= 0x00,
        TRUE = 0xFF
} Boolean;

typedef char* uEString;
typedef void* uEObject;

typedef void* TStringList;

#ifndef NULL
#define NULL ((void*)0)
#endif // NULL


/**
 * Adds a new string to the list.
 *
 * @param StringList
 * @param String
 * @return Item position in the list.
 */
int Add(TStringList StringList, const uEString String);

/**
 * Adds a string to the list, and associates an object with the string.
 *
 * @param StringList
 * @param String
 * @param Object
 * @return Item position in the list.
 */
int AddObject(TStringList StringList, const uEString String, uEObject Object);

/**
 * Deletes all the strings from the list.
 *
 * @param StringList
 */
void Clear(TStringList StringList);

/**
 * Removes the string specified by the Index parameter.
 *
 * @param StringList
 * @param Index
 */
void Delete(TStringList StringList, int Index);

/**
 * Swaps the position of two strings in the list.
 *
 * @param StringList
 * @param Index1
 * @param Index2
 */
void Exchange(TStringList StringList, int Index1, int Index2);

/**
 * Locates the index for a string in a sorted list and indicates
 *  whether a string with that value already exists in the list.
 *
 * @param StringList
 * @param String
 * @param Index
 * @return
 */
Boolean Find(TStringList StringList, const uEString String, int *Index);

/**
 * Sets sorted list property. Calling this function
 *  is necessary before using some functions like Find.
 *
 * @param StringList
 */
void SetSorted(TStringList StringList);

/**
 * @param StringList
 * @return
 */
Boolean GetSorted(TStringList StringList);

/**
 * Returns the position of a string in the list.
 *
 * Call IndexOf to obtain the position of the first occurrence of a string
 * that matches String. SA string matches String if it is identical to String.
 * If the string does not have a match in the string list, IndexOf returns -1.
 *
 * IndexOf will work in this way on the condition that Sorted is set to False.
 * This reflects the internal definition of IndexOf, which calls Find if Sorted
 * is set to True and will locate any string in the list that matches the
 * parameter String.
 *
 *
 * @param StringList
 * @param String
 * @return
 */
int IndexOf(TStringList StringList, const uEString String);

/**
 * Inserts a string to the list at the position specified by Index.
 *
 * Call Insert to add the string S to the list at the position specified by Index.
 * If Index is 0, the string is inserted at the beginning of the list.
 * If Index is 1, the string is put in the second position of the list, and so on.
 *
 * If the string has an associated object, use the InsertObject method instead.
 * If the list is sorted, calling Insert or InsertObject will
 * raise an UETSL_ERROR_FORBIDEN_ON_SORTED_LIST error. Use Add or AddObject with
 * sorted lists.
 *
 * @param StringList
 * @param Index
 * @param String
 */
void Insert(TStringList StringList, int Index, const uEString String);

/**
 * Inserts a string into the list at the specified position, and associates it with an object.
 *
 * Call InsertObject to insert the String into the list at the position identified
 * by Index, and associate it with the object Object.
 * If Index is 0, the string is inserted at the beginning of the list. If Index is 1,
 * the string is put in the second position of the list, and so on.
 *
 * If the list is sorted, calling Insert or InsertObject will
 * raise an UETSL_ERROR_FORBIDEN_ON_SORTED_LIST error. Use Add or AddObject with
 * sorted lists.
 *
 * @param StringList
 * @param Index
 * @param String
 * @param Object
 */
void InsertObject(TStringList StringList,
                int Index,
                const uEString String,
                uEObject Object);

/**
 * Sorts the strings in the list in ascending order.
 *
 * Call Sort to sort the strings in a list that has the Sorted
 * property set to false.
 * String lists with the Sorted property set to true are automatically sorted.
 *
 * @param StringList
 */
void Sort(TStringList StringList);

/**
 * Creates new string list.
 */
TStringList Create(void);

/**
 * Destroys list and frees memory
 *
 * @param StringList
 */
void Destroy(TStringList *StringList);

/**
 * Retrieves error code of the last operation.
 */
uEStringListError Error(void);

/**
 * Retrieves string from item at Index
 *
 * @param StringList
 * @param Index
 * @return
 */
const uEString
GetString(TStringList StringList, int Index);

/**
 * Retrieves stored object from item at Index
 *
 * @param StringList
 * @param Index
 * @return
 */
const uEObject
GetObject(TStringList StringList, int Index);

#endif // T_UE_STRING_LIST_H
